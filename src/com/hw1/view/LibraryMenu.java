package com.hw1.view;

import java.util.Scanner;

import com.hw1.model.dto.AniBook;
import com.hw1.model.dto.Book;
import com.hw1.model.dto.CookBook;
import com.hw1.model.dto.Member;

public class LibraryMenu {
	private LibraryManager lm = new LibraryManager();
	Scanner sc = new Scanner(System.in);
	
	public LibraryMenu() {}
	
	public void mainMenu() {
		System.out.print("이름 : ");
		String name = sc.next();
		System.out.print("나이 : ");
		int age = sc.nextInt();
		System.out.print("성별(M/W) : ");
		char gender = Character.toUpperCase(sc.next().charAt(0));
		Member member = new Member(name, age, gender);
		
		lm.insertMember(member);
		
		while(true) {
			System.out.println("==== 메뉴 ====");
			System.out.println("1. 마이페이지");
			System.out.println("2. 도서 전체 조회");
			System.out.println("3. 도서 검색");
			System.out.println("4. 도서 대여하기");
			System.out.println("0. 프로그램 종료하기");
			
			System.out.print("\n숫자 입력 : ");
			int num = sc.nextInt();
			switch (num) {
			case 1: 
				System.out.println("\n"+lm.myInfo().toString()+"\n");
				break;
			case 2:
				selectAll();
				break;
			case 3:
				searchBook();
				break;
			case 4:
				rentBook();
				break;
			case 0:
				System.out.println("시스템 종료");
				return;
			default:
				System.out.println("다시 입력해주세요.");
				continue;
			}
			
		}
		
	}
	
	public void selectAll() {
		System.out.println();
		Book[] bList = lm.selectAll();
		for(int i = 0; i < bList.length; i++) {
			if(bList[i] instanceof CookBook) {
				CookBook cookBook = (CookBook)bList[i];
				System.out.println(i + "번도서 : " + cookBook.toString());
			}
			if(bList[i] instanceof AniBook) {
				AniBook aniBook = (AniBook)bList[i];
				System.out.println(i + "번도서 : " + aniBook.toString());
			}
		}
		System.out.println();
	}
	
	public void searchBook() {
		sc.nextLine();
		System.out.print("\n검색할 제목 키워드 : ");
		String keyword = sc.nextLine();
		Book[] searchList = lm.searchBook(keyword);

		System.out.println();
		int idx = 0;
		for(Book sb : searchList) {
			if(sb != null) {
				if(sb instanceof CookBook) {
					CookBook cookBook = (CookBook)sb;
					System.out.println(idx + "번도서 : " + cookBook.toString());
				} 
				if(sb instanceof AniBook) {
					AniBook aniBook = (AniBook)sb;
					System.out.println(idx + "번도서 : " + aniBook.toString());
				} 
			} else {
				if(idx == 0) {
					System.out.println("검색한 도서가 존재하지 않습니다.");
				}
			}
			idx++;
		}
		System.out.println();
		
	}
	
	public void rentBook() {
		selectAll();
		System.out.print("대여할 도서 번호 선택 : ");
		int index = sc.nextInt();
		int result = lm.rentBook(index);
		
		switch (result) {
		case 0:
			System.out.println("\n성공적으로 대여되었습니다.\n");
			break;
		case 1:
			System.out.println("\n나이 제한으로 대여 불가능입니다.\n");
			break;
		case 2:
			System.out.println("\n성공적으로 대여되었습니다. 요리학원 쿠폰이 발급되었습니다. 마이페이지를 통해 확인하세요\n");
			break;
		default:
			break;
		}
	}
}
